# -*- coding: utf-8 -*- 

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################  
def index():
    if session.id:
        redirect(URL(r=request,f='welcome'))
    return dict()

def user():
    """
    exposes:
    http://..../[app]/default/user/login 
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
#    newslot=db.user[session.id].download_slots-1
#   db(db.user.id==session.id).update(download_slots=newslot)
    return response.download(request,db)
def download2():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    if(db.user[session.id].download_slots>0):
      newslot=db.user[session.id].download_slots-1
      db(db.user.id==session.id).update(download_slots=newslot)
      return response.download(request,db)
    redirect(URL(r=request,f='all_songs_detail',args=session.songid))

def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    session.forget()
    return service()

# Used For checking password and retype password are same or not.   
def passwordcheck(form):        
    if form.vars.password!=form.vars.retype_password:
        form.errors.password="Passwords do not match"
        form.errors.retype_password="Passwods do not match"
# Used for registering of user to music library
def register():
    if session.id:
        redirect(URL(r=request,f='welcome'))
    db.user.type.default=2
    db.user.visits.default=1
    db.user.download_slots.default=10
    db.user.slots_used.default=0         # Of no use
    db.user.rates.default=0
    db.user.rates.readable=db.user.rates.writable=False
    db.user.download_slots.readable=db.user.download_slots.writable=False
    db.user.slots_used.readable=db.user.slots_used.writable=False
    db.user.type.readable=db.user.type.writable=False
    db.user.visits.readable=db.user.visits.writable=False
#db.user.download_slots.readable=db.download_slots.writable=False
    form=SQLFORM(db.user)
    if(form.accepts(request.vars,session,onvalidation=passwordcheck)):
#if(form.vars.password!=form.vars.retype_password):
        session.flash="Successfully Registered"
#           redirect(URL(r=request,f="register"))
        redirect(URL(r=request,f='confirmation'))
    if form.errors:
        response.flash=T("Form has errors")
    return dict(form=form)
# Used if a user forgots his password
def forgot_password():
    redirect(URL(r=request,f='index'))

# Used for logging in to library
def login():
    if(session.id):
        redirect(URL(r=request,f='welcome'))
    if(request.vars.email and request.vars.password):
        session.email=request.vars.email
        session.password=request.vars.password
        redirect(URL(r=request,f='check'))
#if (session.error):
#       return dict()
    else:
        session.error='empty'
        return dict()

# Used to check username and password for a user while logging in
def check():
    users=db(db.user.id>0).select()
    if users:
        for user in users:
            if(user.email_id==session.email):
                if(user.password==session.password):        
#                   if(user.type==4):
#                   session.flash='Your request is awaiting confirmation'
#                   redirect(URL(r=request,f='login'))
#               else:
                    import datetime
                    session.user=user.type
                    session.id=user.id
                    name=db(db.user.id==session.id).select()
                    session.name=name[0].first_name
#                   date=db(user.id==db.date.userid).select()
#       if date:
#                       db(db.date.userid==user.id).update(lastdate=datetime.datetime.today())
#                   else:
#                       db.date.insert(userid=user.id,lastdate=datetime.datetime.today())
                    session.logintime=request.now
                    session.flash=T('Welcome ' + session.name)
                    redirect(URL(r=request,f='welcome'))
#       session.flash="Invalid Email/Password"
        redirect(URL(r=request,f='index'))
    else:
        redirect(URL(r=request,f='index'))
# First page after Logging in.Shows User his details and his playlists
def welcome():
    if session.id:
        userid=session.id
        user=db(db.user.id==userid).select()
        uploaded=db(db.music.userid==userid).select()
        playlist=db(db.playlist.userid==session.id).select()
        return dict(user=user[0],uploaded=uploaded,playlist=playlist)
    redirect(URL(r=request,f='index'))
# Stores Filename of music file
def filename(form):
    form.vars.filename=request.vars.song.filename
# Used to upload new music gile
def add_music():
    if (session.id):
        response.flash=T('Upload Music')
        db.music.type.default=2
        db.music.rating.default=3
        db.music.userid.default=session.id
        db.music.type.readable=db.music.type.writable=False
        db.music.userid.readable=db.music.userid.writable=False
        form=SQLFORM(db.music,submit_button='Upload')
        #if 'song' in request.vars:
#           form.vars.filename=request.vars.song.filename
        #form.vars.filename='fasfsa'
#   redirect(URL(r=request,f='welcome'))
        if(form.accepts(request.vars,session,onvalidation=filename)):
            response.flash='music added'
            redirect(URL(r=request,f='add_music'))
        return dict(form=form)
    redirect(URL(r=request,f='index'))
# Used for editing music details...only for administrator
def edit_music():
    if(session.id and session.user==1):
        response.flash=T('Edit Music')
        songid=session.songid
        db.music.userid.writable=False
        url=URL(r=request,f='download')
        song=SQLFORM(db.music,songid,upload=url)
        if song.accepts(request.vars,session):
            redirect(URL(r=request,f="all_songs_detail",args=songid))
        return dict(song=song)
    redirect(URL(r=request,f='index'))
# Used for deleting music from library....only for administrator
def delete_music():
    if(session.id and session.user==1):
        songid=session.songid
        db(db.music.id==songid).delete()
        db(db.playlist_song.songid==songid).delete()
        db(db.song_rating.songid==songid).delete()
        db(db.keyword.songid==songid).delete()
        db(db.comments.songid==songid).delete()
        db(db.user_rating.songid==songid).delete()
        redirect(URL(r=request,f='all_songs'))
    redirect(URL(r=request,f='index'))
def user_history():
    if(session.id and session.user==1):
        history=db(db.usrhistory.id>0).select(db.usrhistory.ALL,orderby=~db.usrhistory.login_time)
        totalvisits=db.user.visits.sum()
        return dict(history=history,totalvisits=totalvisits)
    redirect(URL(r=request,f='index'))
             
# Used for adding keyword for a music....only for administrator
def add_keyword():
    if(session.id and session.user==1):
        songid=request.args[0] or redirect(URL(r=request,f='welcome'))
#   music=db(db.music.id==songid).select()
#       form=crud.read(db.music,songid)
        db.keyword.songid.default=songid
        db.keyword.songid.readable=db.keyword.songid.writable=False
#       db.keyword.type.readable=db.keyword.item_id.writable=False
        db.keyword.keyword.requires=IS_NOT_IN_DB(db(db.keyword.songid==songid),'keyword.keyword',error_message=T('Aleady added'))
        form=SQLFORM(db.keyword)
        if(form.accepts(request.vars,session)):
            response.flash=T('Keyword added')
        return dict(form=form)
    redirect(URL(r=request,f='index'))
# Used for displaying songs uploaded by particular user
def user_songs():
    if(session.id):
        userid=session.id
        songs=db(db.music.userid==userid).select()
        return dict(songs=songs)
    redirect(URL(r=request,f='index'))
# Used for displaying all songs in library
def all_songs():
    if session.id:
        response.flash=T('Song Library')
        songs=db(db.music.type==1).select(orderby=db.music.title)
        return dict(songs=songs)
    redirect(URL(r=request,f='index'))
# Used for dsiplaying details of a particular song
def all_songs_detail():
    if session.id:
        songid=request.args[0] or redirect(URL(r=request,f='welcome'))
        response.flash=T(db.music[songid].title)
        session.songid=songid
        userid=db.music[songid].userid
        user=db(db.user.id==userid).select()
        details=db(db.music.id==songid).select()
        rating=db(db.song_rating.songid==songid).select()
        return dict(user=user,details=details,rating=rating)
    redirect(URL(r=request,f='index'))
# Used for notifications of new music added to library by users
def music_notification():
    if(session.id and session.user==1):
        response.flash=T('Pending requests')
        song=db(db.music.type==2).select()
#session.pending=db(db.music.type==2).count()
        return dict(song=song)
    redirect(URL(r=request,f='index'))
# Used for showing details of new music 
def notification_detail():
    if(session.id and session.user==1):
        response.flash=T('Pending Song Details')
        songid=request.args[0] or redirect(URL(r=request,f='welcome'))
        session.songid=songid
        userid=db.music[songid].userid
        user=db(db.user.id==userid).select()
        details=db(db.music.id==songid).select()
#details=SQLFORM(db.music,songid)
        return dict(details=details,user=user)
    redirect(URL(r=request,f='index'))
# Used for Accepting a new music added by users
def music_accept():
    if(session.id and session.user==1):
        songid=session.songid
        db(db.music.id==songid).update(type=1)
        movie=db.music[songid].movie
        title=db.music[songid].title
        artist=db.music[songid].artist
        yor=db.music[songid].yor
        if movie:
            db.keyword.insert(songid=songid,keyword=movie)
        if title:
            db.keyword.insert(songid=songid,keyword=title)
        if artist:
            db.keyword.insert(songid=songid,keyword=artist)
        if yor:
            db.keyword.insert(songid=songid,keyword=str(yor))
        db.song_rating.insert(songid=songid,rating_sum=db.music[songid].rating,users_rated=1,avg_rating=db.music[songid].rating)
        db.user_rating.insert(userid=db.music[songid].userid,songid=songid,rating=db.music[songid].rating)
        redirect(URL(r=request,f='music_notification'))
    redirect(URL(r=request,f='index'))
# Used for Declining a music uploaded by user
def music_decline():
    if(session.id and session.user==1):
        songid=session.songid
        db(db.music.id==songid).delete()
        redirect(URL(r=request,f='music_notification'))
    redirect(URL(r=request,f='index'))
# Used for playing a song
def playsong():
    if session.id:
        songid=request.args[0] or redirect(URL(r=request,f='welcome'))
        filename=db.music[songid].song
        return dict(filename=filename,songid=songid)
    redirect(URL(r=request,f='index'))
# used for creating a new playlist by a user
def create_playlist():
    if session.id:
        userid=session.id
        db.playlist.userid.default=userid
        db.playlist.userid.readable=db.playlist.userid.writable=False
        db.playlist.name.requires=IS_NOT_IN_DB(db(db.playlist.userid==session.id),'playlist.name',error_message=T('Playlist already exists!!!'))
        form=SQLFORM(db.playlist)
        if (form.accepts(request.vars,session)):
            redirect(URL(r=request,f='create_playlist'))
        return dict(form=form)
    redirect(URL(r=request,f="index"))
# Used for showing playlists of a paticular user
def user_playlist():
    if session.id:
        playlist=db(db.playlist.userid==session.id).select()
        return dict(playlist=playlist)
    redirect(URL(r=request,f='index'))
# Used for showings songs in a particular playlist.
def show_playlist():
    if session.id:
        playlistid=request.args[0] or redirect(URL(r=request,f='create_playlist'))
        session.playlistid=playlistid
        song=db(db.playlist_song.playlistid==playlistid).select()
        return dict(song=song,playlistid=playlistid)
    redirect(URL(r=request,f='index'))
# Used for checking if song is already present in playlist or not
def song_present(form):
    playlist=db(db.playlist_song.playlistid==form.vars.playlistid).select()
    present='no'    
    for song in playlist:
        if (song.songid==int(session.songid)):
            present='yes'
    if (present=='yes'):
        form.errors.playlistid='Already in playlist'
# Used for adding a new song to playlist
def addsong_playlist():
    if session.id:
#songid=request.args[0] or redirect(URL(r=request,f='create_playlist'))
#playlistid=request.args[0] or redirect(URL(r=request,f='create_playlist'))
        songid=session.songid
        db.playlist_song.songid.default=songid
        db.playlist_song.songid.readable=db.playlist_song.songid.writable=False
        db.playlist_song.playlistid.requires=IS_IN_DB(db(db.playlist.userid==session.id),'playlist.id','%(name)s')
        form=SQLFORM(db.playlist_song)
        if form.accepts(request.vars,session,onvalidation=song_present):
            redirect(URL(r=request,f='addsong_playlist',args=songid))
        return dict(form=form)
    redirect(URL(r=request,f='index'))
# Used for deleting a song from playlist
def deletesong_playlist():
    if session.id:
        songid=request.args[0] or redirect(URL(r=request,f='create_playlist'))
        query1=db.playlist_song.playlistid==session.playlistid
        query2=db.playlist_song.songid==songid
        db(query1 and query2).delete()
        redirect(URL(r=request,f='show_playlist',args=session.playlistid))
    redirect(URL(r=request,f='index'))
# Used for deleting a playlist
def delete_playlist():
    if session.id:
        playlistid=request.args[0] or redirect(URL(r=request,f='welcome'))
        db(db.playlist.id==playlistid).delete()
        db(db.playlist_song.playlistid==playlistid).delete()
        redirect(URL(r=request,f='user_playlist'))
    redirect(URL(r=request,f='index'))
def show_movie():
    if session.id:
        response.flash=T('Movie List')
        movie=db(db.music.id>0).select(db.music.movie,distinct=True)
        return dict(movie=movie)
    redirect(URL(r=request,f='index'))
def show_moviesong():
    if session.id:
        songid=request.args[0] or redirect(URL(r=request,f='welcome'))
        movie=db.music[songid].movie
        song=db((db.music.movie==movie) & (db.music.type==1)).select()
        return dict(song=song)
    redirect(URL(r=request,f='index'))
# Used for checking if user already rated the song or not
def rating_confirm(form):
    rated=db(db.user_rating.userid==session.id).select()
    rate=1  
    for song in rated:
        if (song.songid==int(session.songid)):
            rate=0
    if (rate==0):
        form.errors.rating='Already rated'
# Used for rating a song by a user
def rate_song():
    if session.id:
        songid=request.args[0] or redirect(URL(r=request,f="welcome"))
        session.songid=songid
        userid=session.id
        db.user_rating.userid.default=userid
        db.user_rating.songid.default=songid
        db.user_rating.userid.readable=db.user_rating.userid.writable=False
        db.user_rating.songid.readable=db.user_rating.songid.writable=False
        form=SQLFORM(db.user_rating)
        if form.accepts(request.vars,session,onvalidation=rating_confirm):
            rating=db(db.song_rating.songid==songid).select()
            rating_sum=rating[0].rating_sum+int(request.vars.rating)
            users_rated=rating[0].users_rated+1
            avg_rating=rating_sum/float(users_rated)
            db(db.song_rating.songid==songid).update(rating_sum=rating_sum)
            db(db.song_rating.songid==songid).update(users_rated=users_rated)
            db(db.song_rating.songid==songid).update(avg_rating=avg_rating)
            avail=db.user[session.id].rates+1
            if(avail==2):
                newslot=db.user[session.id].download_slots+1
                db(db.user.id==session.id).update(download_slots=newslot)
                db(db.user.id==session.id).update(rates=0)
            else:
                db(db.user.id==session.id).update(rates=avail)
#db(db.user.id==session.id).update()
            redirect(URL(r=request,f='rate_song',args=songid))
        return dict(form=form)
    redirect(URL(r=request,f='index'))
# used for showing comments on a song and inserting a new comment....
def comment():
    if session.id:
        songid=request.args[0] or redirect(URL(r=request,f='welcome'))
#       db.comments.userid.default=session.id
        comment=db(db.comments.songid==songid).select(db.comments.ALL,orderby=~db.comments.posted_on)
#       db.comments.songid.default=songid
#       db.comments.userid.readable=db.comments.userid.writable=False
#       db.comments.songid.readable=db.comments.songid.writable=False
#       if form.accepts(request.vars,session):
#           response.flash("Comment Posted !!")
        return dict(songid=songid,comment=comment)
def add_comment():
    if session.id:
        songid=request.args[0]
        db.comments.insert(userid=session.id,songid=songid,posted_on=request.now,comment=request.vars.comment)
        session.flash=T("Comment Posted")
        redirect(URL(r=request,f='comment',args=songid))
    redirect(URL(r=request,f='index'))
def delete_comment():
    if session.id:
        commentid=request.args[0]
        songid=db.comments[commentid].songid
        db.comments[commentid].delete()
        redirect(URL(r=request,f='comment',args=songid))
    redirect(URL(r=request,f='index'))
# Used for searching of a song..
def search():
    if session.id:
        if request.vars.search:
            keyword=request.vars.search
            result=db(db.keyword.keyword.like('%%%s%%' %keyword)).select(db.keyword.songid,distinct=True)
            return dict(result=result,keyword=request.vars.search)
        result=None
        keyword=None
        return dict(result=result,keyword=keyword)
    redirect(URL(r=request,f='index'))
# Used for showing top rated songs..
def top_rated():
#   if session.id:
    top=db(db.song_rating.songid>0).select(orderby=~db.song_rating.avg_rating)
    return dict(top=top)
#redirect(URL(r=request,f='index'))
# Used for contact us details
def contactus():
        response.flash=T('Contact Us')
        return dict()
def confirmation():
    return dict()
# Used for logging out..
def logout():
    if session.id:
        visits=db(db.user.id==session.id).select()
        new=visits[0].visits+1
        db(db.user.id==session.id).update(visits=new)
        visitorno=db.user.visits.sum()
        db.usrhistory.insert(userid=session.id,login_time=session.logintime,logout_time=request.now,visitorno=db().select(visitorno)[0]._extra[visitorno])
        session.clear()
        redirect(URL(r=request,f="index"))
    redirect(URL(r=request,f='index'))
