# -*- coding: utf-8 -*- 

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
#########################################################################

if request.env.web2py_runtime_gae:            # if running on Google App Engine
    db = DAL('gae')                           # connect to Google BigTable
    session.connect(request, response, db=db) # and store sessions and tickets there
    ### or use the following lines to store sessions in Memcache
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db=MEMDB(Client())
else:                                         # else use a normal relational database
    db = DAL('sqlite://storage2.sqlite')       # if not, use SQLite or other DB
## if no need for session
# session.forget()

#########################################################################
## Here is sample code if you need for 
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - crud actions
## comment/uncomment as needed

from gluon.tools import *
auth=Auth(globals(),db)                      # authentication/authorization
auth.settings.hmac_key='sha512:871633f0-1186-44d3-9f20-6797d2e5dd3f'
auth.define_tables()                         # creates all needed tables
crud=Crud(globals(),db)                      # for CRUD helpers using auth
service=Service(globals())                   # for json, xml, jsonrpc, xmlrpc, amfrpc

# crud.settings.auth=auth                      # enforces authorization on crud
# mail=Mail()                                  # mailer
# mail.settings.server='smtp.gmail.com:587'    # your SMTP server
# mail.settings.sender='you@gmail.com'         # your email
# mail.settings.login='username:password'      # your credentials or None
# auth.settings.mailer=mail                    # for user email verification
# auth.settings.registration_requires_verification = True
# auth.settings.registration_requires_approval = True
# auth.messages.verify_email = 'Click on the link http://'+request.env.http_host+URL(r=request,c='default',f='user',args=['verify_email'])+'/%(key)s to verify your email'
# auth.settings.reset_password_requires_verification = True
# auth.messages.reset_password = 'Click on the link http://'+request.env.http_host+URL(r=request,c='default',f='user',args=['reset_password'])+'/%(key)s to reset your password'
## more options discussed in gluon/tools.py
#########################################################################

######################################i###################################
## Define your tables below, for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################
import datetime
#User Table is used to store complete information about the user.
db.define_table('user',			
		Field('first_name','string',requires=IS_NOT_EMPTY(error_message=T("Please Fill your First Name"))),
		Field('last_name','string',requires=IS_NOT_EMPTY(error_message=T("Please Fill your Last Name"))),
		Field('Occupation','string',requires=IS_NOT_EMPTY(error_message=T("Please Fill your Occupation!!"))),
		Field('city','string',requires=IS_NOT_EMPTY(error_message=T("Please Fill your City!!"))),
		Field('country','string',requires=IS_NOT_EMPTY(error_message=T("Please Fill your Country!!"))),
		Field('dob','date',requires=IS_NOT_EMPTY(error_message=T("Please Fill your Date Of Birth!!"))),
		Field('gender','string',requires=IS_IN_SET(['Male','Female']),default='Male',widget=SQLFORM.widgets.radio.widget),
		Field('email_id','string',requires=IS_NOT_EMPTY(error_message=T("Please Enter your E-mail id!!"))),
		Field('password','password',requires=IS_NOT_EMPTY(error_message=T("Please choose a Password!!"))),
		Field('retype_password','password',requires=IS_NOT_EMPTY(error_message=T("Please Retype your Password!!"))),
		Field('download_slots','integer'),# No. of download slots available to user.
		Field('rates','integer'),
		Field('slots_used','integer'),
		Field('visits','integer'),
		Field('type','double'))		#type 1=moderator,2=general user
# Music Table is used to store details of a song.
db.define_table('music',
		Field('userid',db.user),
		Field('movie','string'),
		Field('title','string',requires=IS_NOT_EMPTY()),
		Field('artist','string'),
		Field('type','integer'),       # type 1=music approved,type2=music not approved
		Field('yor','integer'),
		Field('image','upload',autodelete=True,requires=IS_IMAGE()),
		Field('rating','integer',requires=IS_IN_SET([1,2,3,4,5])),
		Field('filename','string'),
		Field('song','upload',autodelete=True,requires=IS_NOT_EMPTY(error_message=T('Please upload the song !!'))))
# keyword table is used to store keywords for a particular song.
db.define_table('keyword',
		Field('songid',db.music),
		Field('keyword','string',requires=IS_NOT_EMPTY()))
# song rating table is used to store ratings for music.
db.define_table('song_rating',
		Field('songid',db.music),
		Field('rating_sum','integer'),
		Field('users_rated','integer'),
		Field('avg_rating','double'))
#user rating is used to store ratings given to a song by a  particular user.
db.define_table('user_rating',
		Field('userid',db.user),
		Field('songid',db.music),
		Field('rating','integer',requires=IS_IN_SET([1,2,3,4,5]),comment='Out of 5'))
#playlist is used to store playlists of a particluar user
db.define_table('playlist',
		Field('userid',db.user),
		Field('name','string',requires=IS_NOT_EMPTY(error_message=T('Please Enter a Name !!'))))
# playlist_song is used to store songs in a particular playlist.
db.define_table('playlist_song',
		Field('playlistid',db.playlist),
		Field('songid',db.music))
db.define_table('usrhistory',
		Field('userid',db.user),
		Field('login_time','datetime'),
		Field('logout_time','datetime'),
		Field('visitorno','integer'))
db.define_table('comments',
		Field('userid',db.user),
		Field('songid',db.music),
		Field('posted_on','datetime'),
		Field('comment','text'))
#db.user.retype_password.requires=IS_NOT_EMPTY(error_message=T('Please Retype Password!!'))
#db.playlist_song.playlistid.requires=IS_IN_DB(db(db.playlist.userid==session.id),'playlist.id','%(name)s')
db.playlist_song.songid.requires=IS_IN_DB(db(db.music.type==1),'music.id','%(title)s')
db.user.email_id.requires=IS_NOT_IN_DB(db,'user.email_id'),IS_EMAIL(error_message=T("Enter Corect E-mail ID"))
db.user.dob.requires=IS_DATE(error_message=T("Enter Date as yyyy-mm-dd"))
db.song_rating.requires=IS_NOT_EMPTY()
db.playlist.name.requires=IS_NOT_IN_DB(db(db.playlist.userid==session.id),'playlist.name')
