# -*- coding: utf-8 -*- 

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.title = request.application
response.subtitle = T('')
response.meta.author = 'you'
response.meta.description = 'describe your app'
response.meta.keywords = 'bla bla bla'

##########################################
## this is the main application menu
## add/remove items as required
##########################################

response.menu = [
    (T('Home'), False, URL(request.application,'default','index'), [])
    ]

##########################################
## this is here to provide shortcuts
## during development. remove in production 
##
## mind that plugins may also affect menu
##########################################
response.menu+=[

     (T('Top Rated'), False, 
          URL(request.application, 'default', 'top_rated')),
]

if not session.id:
    response.menu+=[

   
            
    
    
                 
   
    (T('Contact Us'), False, 
          URL(request.application, 'default', 'contactus')), 
          
                    
            ]

else:            
     response.menu+=[
     
     (T('All Songs'), False, 
       URL(request.application, 'default', 'all_songs')),
    
     (T('All Movies'), False, 
       URL(request.application, 'default', 'show_movie')),             
     
     (T('Upload Song'), False, 
               URL(request.application, 'default', 'add_music')),
     (T('Contact Us'), False, 
          URL(request.application, 'default', 'contactus')), 
     (T('Logout'), False, 
          URL(request.application, 'default', 'logout')),
          
      
     
     ]
     
if(session.user==1):   
    response.menu+=[
           (T('User History'), False, 
               URL(request.application, 'default', 'user_history')),
          (T('Notification (%s)'%db(db.music.type==2).count()), False, 
               URL(request.application, 'default', 'music_notification')),
     

    ]
