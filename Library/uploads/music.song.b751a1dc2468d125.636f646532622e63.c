#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdlib.h>

int main()
{
	pid_t pid=fork();
	if (pid==0)
	{
		//char *args[]={(char *) 0};
		printf("child process initiated\n");
		execlp('rishabh.o',(char *) 0);
		perror("error:");
	}
	else if(pid<0)
	{
		printf("child process not initiated\n");
		exit(0);
	}
	else
	{
		printf("parent process\n");
	}
}
