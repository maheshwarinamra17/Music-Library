#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdlib.h>

int main(int argc,char *argv[])
{
	int pid=fork();
	if(pid==0)
	{
		char *args[]= {"argv[1]",2,3, (char *) 0};
		printf("Child Initiated\n");
		execv(argv[1],args);
	}
	else if(pid<0)
	{
		printf("Child Process not initiated\n");
		exit(0);
	}
	else
	{
		printf("Parent Process\n");
		wait();
	}
}
